import streamlit as st

# Collapse sidebar on start
st.set_page_config(initial_sidebar_state="collapsed",layout="wide")

# Remove sidebar via CSS
st.markdown(
    """
<style>
    [data-testid="collapsedControl"] {
        display: none
    }
</style>
""",
    unsafe_allow_html=True,
)

# st.title("Car Bazaar")

col1, col2 = st.columns(2,gap="small")

with col1:
	st.image("images/buy_now.png")

with col2:
	st.image("images/sell_ride.png")
